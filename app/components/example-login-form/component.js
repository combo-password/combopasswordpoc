import Component from '@ember/component';
import { get, set } from '@ember/object';
import { isPresent } from '@ember/utils';
import { attribute, tagName } from '@ember-decorators/component';
import { action, computed, observes } from '@ember-decorators/object';
import { localClassName, localClassNames } from 'ember-css-modules';
import styles from './styles';

@tagName('form')
@localClassNames('login-form')
export default class ExampleLoginFormComponent extends Component.extend() {

  passwordInput = [];

  checkSequence = false;

  password = [];

  match = null;

  @computed('password.[]')
  get _password() {
    return this.password.map(combo => combo.map(keyPress => keyPress.key).join('')).join('');
  }

  @action
  reset() {
    set(this, 'password', []);
    set(this, 'passwordInput', []);
    set(this, 'match', null);

    this.element.getElementsByClassName(styles['password'])[0].focus();
  }

  @attribute
  @action
  onSubmit(event) {
    if (isPresent(event)) {
      event.preventDefault();
    }

    const password = get(this, 'password');
    const passwordInput = get(this, 'passwordInput');
    const checkSequence = get(this, 'checkSequence');

    if (isPresent(password) && isPresent(passwordInput)) {
      const match = this.compareComboPw(password, passwordInput, checkSequence);
      set(this, 'match', match);
    } else if (isPresent(passwordInput)) {
      set(this, 'password', passwordInput);
      set(this, 'passwordInput', []);
    }

    this.element.getElementsByClassName(styles['password'])[0].focus();
  }

  compareComboPw(a, b, checkSequence) {
    if (a.length !== b.length) {
      return false;
    } else if (checkSequence) {
      return a.every((combo, i) =>
        combo.every((keyPressA, j) =>
          b[i][j].keyCode === keyPressA.keyCode
        )
      );
    }

    return a.every((combo, i) =>
      combo.every((keyPressA, j) =>
        b[i].some(keyPressB => keyPressB.keyCode === keyPressA.keyCode)
      )
    );
  }

  @localClassName
  @computed('match')
  get greenShadow() {
    if (isPresent(this.match) && this.match) {
      const passwordElement = this.element.getElementsByClassName(styles['password'])[0];

      if (isPresent(passwordElement)) {
        passwordElement.classList.remove(styles['red-shadow'],styles['orange-shadow']);
        passwordElement.classList.add(styles['green-shadow']);
      }
      return true;
    }

    return false;
  }

  @localClassName
  @computed('match')
  get orangeShadow() {
    if (!isPresent(this.match)) {
      const passwordElement = this.element.getElementsByClassName(styles['password'])[0];

      if (isPresent(passwordElement)) {
        passwordElement.classList.remove(styles['red-shadow'],styles['green-shadow']);
        passwordElement.classList.add(styles['orange-shadow']);
      }
      return true;
    }

    return false;
  }

  @localClassName
  @computed('match')
  get redShadow() {
    if (isPresent(this.match) && !this.match) {
      const passwordElement = this.element.getElementsByClassName(styles['password'])[0];

      if (isPresent(passwordElement)) {
        passwordElement.classList.remove(styles['green-shadow'],styles['orange-shadow']);
        passwordElement.classList.add(styles['red-shadow']);
      }
      return true;
    }

    return false;
  }

  @observes('passwordInput.[]')
  resetShadow() {
    set(this, 'match', null);
  }

}
