import Component from '@ember/component';
import { argument } from '@ember-decorators/argument';
import { optional } from '@ember-decorators/argument/types';
import { attribute } from '@ember-decorators/component';
import { computed } from '@ember-decorators/object';

export default class ComboPwInputOverlayComponent extends Component {

  @argument(Array)
  password = [];

  @argument(optional(Object))
  onSubmit() {}

  @argument(optional('string'))
  @attribute
  placeholder = "";

  @argument(optional('string'))
  @attribute
  type = "password";

  @argument(optional('string'))
  inputClass = "";

  @argument(optional('boolean'))
  readonly = false;

  @argument(optional('boolean'))
  required = true;

  buffer = [];

  @computed('buffer.[]')
  get bufferString() {
    return this.buffer.map(keyPress => keyPress.key).join('');
  }
}

